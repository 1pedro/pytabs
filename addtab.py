from gi.repository import Gtk

class Tab():
	def __init__(self):
		builder = Gtk.Builder()
		builder.add_from_file('new_tab.ui')

		button = builder.get_object('bSave')
		button.connect('clicked', self.onButtonSavePressed)

		button2 = builder.get_object('bCancel')
		button2.connect('clicked', self.onButtonCancelPressed)

		textarea = builder.get_object('tvTab')
		textarea.set_size_request(600, 600)

		window = builder.get_object('window1')
		window.connect("delete-event", lambda w, e: w.hide() or True)
		window.show_all()

	def onButtonSavePressed(self, button):
		print('Save tab')

	def onButtonCancelPressed(self, button):
		print('Cancel')
