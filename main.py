from gi.repository import Gtk

import sqlite3
import addtab


connection = sqlite3.connect('tabs.db')
c = connection.cursor()


c.execute('CREATE TABLE IF NOT EXISTS tabs (_id integer, artista text, musica text)')

class AppWindow():
	def __init__(self):
		builder = Gtk.Builder()
		builder.add_from_file('window.ui')

		button = builder.get_object('btn_addTab')
		button.connect('clicked', self.onButtonPressed)

		button2 = builder.get_object('btn_listTabs')
		button2.connect('clicked', self.onButton2Pressed)

		window = builder.get_object('window1')
		window.resize(200, 200)
		window.connect("delete-event", Gtk.main_quit)
		window.show_all()
		Gtk.main()


	def onButtonPressed(self, button):
		tabs = addtab.Tab()

	def onButton2Pressed(self, button):
		win_list = ListTabs()
		win_list.show_all()


class ListTabs(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="Tabs")
		store = Gtk.ListStore(int,str,str)

		for i in c.execute('SELECT * FROM tabs'):
			print "Id: %i Artista: %s Musica: %s" %(int(i[0]), i[1], i[2])
			teste = store.append([i[0], i[1], i[2]])


		#view
		tree = Gtk.TreeView(store)
		renderer = Gtk.CellRendererText()

		columnid = Gtk.TreeViewColumn('Id', renderer, text=0)
		tree.append_column(columnid)

		columnband = Gtk.TreeViewColumn('Banda', renderer, text=1)
		tree.append_column(columnband)

		columnmusic = Gtk.TreeViewColumn('Musica', renderer, text=2)
		tree.append_column(columnmusic)
		self.add(tree)



app = AppWindow()
